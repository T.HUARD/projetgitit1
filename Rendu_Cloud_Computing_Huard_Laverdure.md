# Rendu 

## Tp Paas 

_D'après vous, quelle différence peut-on noter entre le logiciel Wordpress et le service de Saas Wordpress.com :_ 

Le logiciel WordPress est un logiciel de mise en page de site web où l’utilisateur doit lui-même gérer l’hébergement et la publication du site.  Alors que le service Wordpress.com est un service permettant de créer et de mettre en page un site web tout en l’hébergeant et le rendant accessible sur le net. 


_Quels éléments (vu dans le cours) faudrait-il mettre en place pour proposer notre propre service de Saas basé sur Wordpress ?_ 

WordPress est un Laas tandis que Wordpress.com est un Saas. Pour proposer une service Saas basée sur Wordpress, il faut ajouter les éléments suivants :  

-Applications 
-Données  
-Runtime 
-Centriciel  
-SE 


_Proposez une architecture pour rendre ce service :_ 


Pour rendre ce service, il faut créer une architecture Paas.  


## Tp Saas 

_Donnez une explication des technologies qui selon vous sont misent en œuvre ici :_

Heroku est un service cloud Platform as a Service (PaaS).  Il permet de développer des applications sur des serveurs accessibles en ligne. Le service cloud permet de compiler et déployer des applications dans des langages spécifiques. 

## Tp Architecture 

_Fournissez un schéma d'architecture complet représentant cette pile applicative :_



![image info](./Diagrammearchi.png)
